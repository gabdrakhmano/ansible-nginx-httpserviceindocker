#!/bin/bash
ansible-playbook -i inventory/mcs/hosts all.yml --vault-password-file ~/.ansible_pass.txt --extra-vars '{"nginx_port": "80"}' -v
